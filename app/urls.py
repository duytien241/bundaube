from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth.views import PasswordResetView, LogoutView
from django.urls import include, path, re_path
from django.contrib import admin
from . import views
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
)

user_detail = views.EditIformationUser.as_view({
    'get': 'retrieve',
    'put': 'update',
})

menu_detail = views.MenuItemIformationUser.as_view({
    'get': 'retrieve',
})

cart_del = views.DeleteCart.as_view({
    'delete': 'destroy'
})

order_detail = views.OrderDetailList.as_view({
    'post': 'create',
    'get': 'list',
})

order_detail_id = views.OrderDetailList.as_view({
    'get': 'retrieve',
})

urlpatterns = [
    path('me', views.Me.as_view(), name='me'),
    path('logout/', views.Logout.as_view()),
    path('rest-auth/', include('rest_auth.urls')),
    path('users', views.Users.as_view(), name='users-list'),
    path('user/<int:pk>/', user_detail, name='user-detail'),
    path('user/changepassword/', views.ChangePassword.as_view(), name='change-password'),
    path('menu/', views.MenuItem.as_view(), name='menu-list'),
    path('menu/<int:pk>/', menu_detail, name='menu_detail'),
    path('orders/', views.OrderHeader.as_view(), name='order-list'),
    path('order/', order_detail, name='order_detail'),
    path('order/<int:order>/', order_detail_id, name='order_detail_id'),
    path('cart/', views.Cart.as_view(), name='cart'),
    path('cart/<int:pk>', cart_del, name='delete-cart'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'xml'])